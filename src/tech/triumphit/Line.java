package tech.triumphit;

import tech.triumphit.utils.GeneratePoints;

import javax.media.opengl.*;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.*;
import java.util.ArrayList;
import java.util.Random;

public class Line implements GLEventListener {

    Random rand = new Random();
    float constant = 100f;
    static ArrayList<GeneratePoints> inputs;


    static GLProfile profile = GLProfile.get(GLProfile.GL2);
    static GLCapabilities capabilities = new GLCapabilities(profile);
    // The canvas
    static GLCanvas glcanvas = new GLCanvas(capabilities);

    public static void main(String[] args) {
        //getting the capabilities object of GL2 profile


        Line l = new Line();
        //creating frame
        glcanvas.addGLEventListener(l);
        glcanvas.setSize(600, 400);

        final JFrame frame = new JFrame("straight Line");
        //adding canvas to frame
        frame.getContentPane().add(glcanvas);
        frame.setSize(frame.getContentPane().getPreferredSize());
        frame.setVisible(true);
        //inputs = GeneratePoints.getInputsFromFile("C:\\Users\\Tushar\\Downloads\\Compressed\\Lab01\\Lab01\\jogl\\inputs.txt");
        inputs = GeneratePoints.getRandomInputs(5);
//        for (int t = 0; t < inputs.size(); t++) {
//            //System.out.println(inputs.get(t).getPoints().getX());
//            //System.out.println(inputs.get(t).getPoints().getY());
//            //System.out.println();
//        }

    }

    public void display(GLAutoDrawable drawable) {

        final GL2 gl = drawable.getGL().getGL2();
        gl.glBegin (GL2.GL_POINTS);

        for (int t = 0; t < inputs.size() - 1; t++) {
            ArrayList<Points> points = getLinePoints(
                    inputs.get(t).getPoints().getX(),
                    inputs.get(t).getPoints().getY(),
                    inputs.get(t + 1).getPoints().getX(),
                    inputs.get(t + 1).getPoints().getY()
            );

            for (int innerT = 0; innerT < points.size(); innerT++) {
                gl.glVertex2d(points.get(innerT).getX(), points.get(innerT).getY());
            }

        }

        gl.glEnd();

    }


    public void dispose(GLAutoDrawable arg0) {

    }


    public void init(GLAutoDrawable drawable) {

    }

    public void reshape(GLAutoDrawable arg0, int arg1, int arg2, int arg3, int arg4) {

    }


    private ArrayList<Points> getLinePoints(float x0, float y0, float x1, float y1) {

        float slop = ((y1 - y0) / (x1 - x0));
        float y = y0;
        float x = x0;

        ArrayList<Points> points = new ArrayList<>();

        if (slop > 0) {

            slop = (1 / slop) * 0.00005f;
            if(y1 < y){
                while(y1 <= y){
                    y1 += 0.00005f;
                    x = x + slop;

                    Points p = new Points();

                    p.setX(x);
                    p.setY(y1);
                    points.add(p);
                }
            }else {
                while(y <= y1){
                    y += 0.00005f;
                    x = x + slop;

                    Points p = new Points();

                    p.setX(x);
                    p.setY(y);
                    points.add(p);
                }
            }

            System.out.println("x0 " + x0 + " y0 " + y0);
            System.out.println("x1 " + x1 + " y1 " + y1);
            System.out.println(slop + " pos");
            System.out.println();
            System.out.println();

        } else if (slop < 0) {

            System.out.println("x0 " + x0 + " y0 " + y0);
            System.out.println("x1 " + x1 + " y1 " + y1);
            System.out.println(Math.abs(slop) * 0.00005f + " min");
            System.out.println();
            System.out.println();

            if(x1 < x){
                while (x1 <= x) {
                    y = y + (Math.abs(slop) * 0.00005f);
                    x1 += 0.00005f;
                    Points p = new Points();
                    p.setX(x1);
                    p.setY(y);
                    points.add(p);
                }
            }else {
                while (x <= x1) {
                    y = y + (Math.abs(slop) * 0.00005f);
                    x += 0.00005f;
                    Points p = new Points();
                    p.setX(x);
                    p.setY(y);
                    points.add(p);
                }
            }


        } else {
            System.out.println(slop + " eq");
        }

        return points;

    }

    private class Points {
        float x, y;

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }
    }

}