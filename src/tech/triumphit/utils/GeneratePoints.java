package tech.triumphit.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class GeneratePoints {
    static ArrayList<GeneratePoints> l;

    private Points points;

    public Points getPoints() {
        return points;
    }

    public void setPoints(Points points) {
        this.points = points;
    }

    public static ArrayList<GeneratePoints> getInputsFromFile(String path) {
        File file = new File(path);
        try {
            l = new ArrayList();
            BufferedReader br = new BufferedReader(new FileReader(file));

            String line;
            while ((line = br.readLine()) != null) {
                Points p = new Points();
                String[] in = line.split(" ");
                if(!line.equals(" ") && !line.equals("")){
                    System.out.println(line);
                    p.setX(Float.parseFloat(in[0]));
                    p.setY(Float.parseFloat(in[1]));

                    GeneratePoints gp = new GeneratePoints();
                    gp.setPoints(p);
                    l.add(gp);
                }

            }


        } catch (FileNotFoundException e) {
            System.out.println(e.toString());
        }catch (NumberFormatException e) {
            System.out.println(e.toString());
        }  catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return l;

    }


    public static ArrayList<GeneratePoints> getRandomInputs(int limit){
        Random rand = new Random();
        float constant = 100f;

        ArrayList<GeneratePoints> points = new ArrayList<>();

        int min = -100; int max = 100;
        for(int t = 0; t < limit; t++){
            int i = rand.nextInt(max + 1 -min) + min;
            int j = rand.nextInt(max + 1 -min) + min;

            float x = i / constant;
            float y = j / constant;

            Points p = new Points();
            p.setX(x);
            p.setY(y);

            GeneratePoints gp = new GeneratePoints();
            gp.setPoints(p);

            points.add(gp);
        }
        return points;
    }

    public static final class Points{
        private float x;
        private float y;

        public float getX() {
            return x;
        }

        public void setX(float x) {
            this.x = x;
        }

        public float getY() {
            return y;
        }

        public void setY(float y) {
            this.y = y;
        }

        @Override
        public String toString() {
            return "Points{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

}
